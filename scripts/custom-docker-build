#!/bin/bash

set -e
IFS=$'\n\t'

source scripts/build-helpers.sh

function print_golang_args() {
    case "$1" in
        1.14)
            INSTALL_GOLANG_VERSION=1.14.15
            GOLANG_DOWNLOAD_SHA256=c64a57b374a81f7cf1408d2c410a28c6f142414f1ffa9d1062de1d653b0ae0d6
            ;;
        1.15)
            INSTALL_GOLANG_VERSION=1.15.10
            GOLANG_DOWNLOAD_SHA256=4aa1267517df32f2bf1cc3d55dfc27d0c6b2c2b0989449c96dd19273ccca051d
            ;;
        1.16)
            INSTALL_GOLANG_VERSION=1.16.2
            GOLANG_DOWNLOAD_SHA256=542e936b19542e62679766194364f45141fde55169db2d8d01046555ca9eb4b8
            ;;
        *) echo "Unknown golang version $1"; exit 1;
    esac

    printf -- "--build-arg INSTALL_GOLANG_VERSION=%s " "$INSTALL_GOLANG_VERSION"
    printf -- "--build-arg GOLANG_DOWNLOAD_SHA256=%s " "$GOLANG_DOWNLOAD_SHA256"
}

# If you add a new minor version here, be sure to check that the
# Chrome versions can be found at https://www.ubuntuupdates.org/pm/google-chrome-stable.
# ChromeDriver supports this: https://sites.google.com/a/chromium.org/chromedriver/downloads.
# You may need to bump the version in scripts/install-chrome.
function print_chrome_args() {
    case "$1" in
        69|69.0)
            CHROME_VERSION=69.0.3497.81-1
            CHROME_DRIVER_VERSION=2.41
            ;;
        71|71.0)
            CHROME_VERSION=71.0.3578.98-1
            CHROME_DRIVER_VERSION=2.45
            ;;
        73|73.0)
            CHROME_VERSION=73.0.3683.103-1
            CHROME_DRIVER_VERSION=73.0.3683.68
            ;;
        74|74.0)
            CHROME_VERSION=74.0.3729.169-1
            CHROME_DRIVER_VERSION=74.0.3729.6
            ;;
        81|81.0)
            CHROME_VERSION=81.0.4044.129-1
            CHROME_DRIVER_VERSION=81.0.4044.69
            ;;
        83|83.0)
            CHROME_VERSION=83.0.4103.61-1
            CHROME_DRIVER_VERSION=83.0.4103.39
            ;;
        84|84.0)
            CHROME_VERSION=84.0.4147.89-1
            CHROME_DRIVER_VERSION=84.0.4147.30
            ;;
        85|85.0)
            CHROME_VERSION=85.0.4183.83-1
            CHROME_DRIVER_VERSION=85.0.4183.87
            ;;
        87|87.0)
            CHROME_VERSION=87.0.4280.88-1
            CHROME_DRIVER_VERSION=87.0.4280.88
            ;;
        89|89.0)
            CHROME_VERSION=89.0.4389.90-1
            CHROME_DRIVER_VERSION=89.0.4389.23
            ;;
        *) echo "Unknown chrome version $1"; exit 1;
    esac
    printf -- "--build-arg CHROME_VERSION=%s " "$CHROME_VERSION"
    printf -- "--build-arg CHROME_DRIVER_VERSION=%s " "$CHROME_DRIVER_VERSION"
}

# see https://www.kernel.org/pub/software/scm/git
function print_git_args() {
    case "$1" in
        2.29)
            GIT_VERSION=2.29.0
            GIT_DOWNLOAD_SHA256=fa08dc8424ef80c0f9bf307877f9e2e49f1a6049e873530d6747c2be770742ff
            ;;
        2.31)
            GIT_VERSION=2.31.0
            GIT_DOWNLOAD_SHA256=bc6168777883562569144d536e8a855b12d25d46870d95188a3064260d7784ee
            ;;
        *) echo "Unknown git version $1"; exit 1;
    esac

    case "$GIT_VERSION" in
        *.rc[0-9])
            GIT_DOWNLOAD_URL=https://www.kernel.org/pub/software/scm/git/testing/git-${GIT_VERSION}.tar.gz;;
        *)
            GIT_DOWNLOAD_URL=https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz;;
    esac

    printf -- "--build-arg GIT_VERSION=%s " "$GIT_VERSION"
    printf -- "--build-arg GIT_DOWNLOAD_SHA256=%s " "$GIT_DOWNLOAD_SHA256"
    printf -- "--build-arg GIT_DOWNLOAD_URL=%s " "$GIT_DOWNLOAD_URL"
}

# see https://github.com/git-lfs/git-lfs/releases
function print_lfs_args() {
    case "$1" in
        2.9)
            LFS_VERSION=2.9.1
            LFS_DOWNLOAD_SHA256=2a8e60cf51ec45aa0f4332aa0521d60ec75c76e485d13ebaeea915b9d70ea466
            ;;
        *) echo "Unknown Git LFS version $1"; exit 1;
    esac

    printf -- "--build-arg LFS_VERSION=%s " "$LFS_VERSION"
    printf -- "--build-arg LFS_DOWNLOAD_SHA256=%s " "$LFS_DOWNLOAD_SHA256"
}

function print_node_args() {
    case "$1" in
        8|8.x) NODE_INSTALL_VERSION=8.16.0 ;;
        10|10.x) NODE_INSTALL_VERSION=10.16.0 ;;
        12|12.x) NODE_INSTALL_VERSION=12.4.0 ;;
        12.18) NODE_INSTALL_VERSION=12.18.4 ;;
        14.15) NODE_INSTALL_VERSION=14.15.4 ;;
        14|14.16) NODE_INSTALL_VERSION=14.16.0 ;;
        *) echo "Unknown node version $1"; exit 1;
    esac
    printf -- "--build-arg NODE_INSTALL_VERSION=%s " "$NODE_INSTALL_VERSION"
}

function print_yarn_args() {
    case "$1" in
        1.12) YARN_INSTALL_VERSION=1.12.3-1 ;;
        1.16) YARN_INSTALL_VERSION=1.16.0-1 ;;
        1.21) YARN_INSTALL_VERSION=1.21.1-1 ;;
        1.22) YARN_INSTALL_VERSION=1.22.5-1 ;;
        *) echo "Unknown yarn version $1"; exit 1;
    esac
    printf -- "--build-arg YARN_INSTALL_VERSION=%s " "$YARN_INSTALL_VERSION"
}

function print_postgres_args() {
    printf -- "--build-arg POSTGRES_VERSION=%s " "$1"
}

function print_docker_args() {
    printf -- "--build-arg DOCKER_VERSION=%s " "$1"
}

function print_graphicsmagick_args() {
    case "$1" in
        1.3.29)
            GRAPHISMAGICK_VERSION=1.3.29
            GRAPHISMAGICK_DOWNLOAD_SHA256=de820cd10597205941a7e9d02c2e679231e92e8e769c204ef09034d2279ad453
            ;;
        1.3.33)
            GRAPHISMAGICK_VERSION=1.3.33
            GRAPHISMAGICK_DOWNLOAD_SHA256=00ea0df7c78c903cce325f402429bcd3924168cf39277f743a0641d47c411ee8
            ;;
        1.3.34)
            GRAPHISMAGICK_VERSION=1.3.34
            GRAPHISMAGICK_DOWNLOAD_SHA256=4717f7a32d964c515d83706fd52d34e089c2ffa35f8fbf43c923ce19343cf2f4
            ;;
        1.3.36)
            GRAPHISMAGICK_VERSION=1.3.36
            GRAPHISMAGICK_DOWNLOAD_SHA256=1e6723c48c4abbb31197fadf8396b2d579d97e197123edc70a4f057f0533d563
            ;;
        *) echo "Unknown graphicsmagick version $1"; exit 1;
    esac

    printf -- "--build-arg GRAPHISMAGICK_VERSION=%s " "$GRAPHISMAGICK_VERSION"
    printf -- "--build-arg GRAPHISMAGICK_DOWNLOAD_SHA256=%s " "$GRAPHISMAGICK_DOWNLOAD_SHA256"
}

function print_pgbouncer_args() {
    case "$1" in
       1.14)
            PGBOUNCER_VERSION=1.14.0
            PGBOUNCER_DOWNLOAD_SHA256=a0c13d10148f557e36ff7ed31793abb7a49e1f8b09aa2d4695d1c28fa101fee7
            ;;
        *) echo "Unknown pgbouncer version $1"; exit 1;
    esac

    printf -- "--build-arg PGBOUNCER_VERSION=%s " "$PGBOUNCER_VERSION"
    printf -- "--build-arg PGBOUNCER_DOWNLOAD_SHA256=%s " "$PGBOUNCER_DOWNLOAD_SHA256"
}

function print_ruby_args() {
    case "$1" in
        2.6|2.6.*)
            RUBY_VERSION="2.6.6"
            RUBY_DOWNLOAD_SHA256="364b143def360bac1b74eb56ed60b1a0dca6439b00157ae11ff77d5cd2e92291"
            ;;

        2.7|2.7.*)
            RUBY_VERSION="2.7.2"
            RUBY_DOWNLOAD_SHA256="6e5706d0d4ee4e1e2f883db9d768586b4d06567debea353c796ec45e8321c3d4"
            ;;

        3.0|3.0.*)
            RUBY_VERSION="3.0.0"
            RUBY_DOWNLOAD_SHA256="a13ed141a1c18eb967aac1e33f4d6ad5f21be1ac543c344e0d6feeee54af8e28"
            ;;

        *) echo "Unknown ruby version $1"; exit 1;
    esac

    printf -- "--build-arg RUBY_VERSION=%s " "$RUBY_VERSION"
    printf -- "--build-arg RUBY_DOWNLOAD_SHA256=%s " "$RUBY_DOWNLOAD_SHA256"
}

function parse_arguments() {
    printf -- "-f Dockerfile.custom "

    # defaults
    CUSTOM_IMAGE_NAME=debian
    CUSTOM_IMAGE_VERSION=buster

    while read image; do
        read version
        case "$image" in
            ruby) print_ruby_args $version ;;
            golang) print_golang_args $version ;;
            chrome) print_chrome_args $version ;;
            docker) print_docker_args $version ;;
            git) print_git_args $version ;;
            lfs) print_lfs_args $version ;;
            node) print_node_args $version ;;
            yarn) print_yarn_args $version ;;
            postgresql) print_postgres_args $version ;;
            graphicsmagick) print_graphicsmagick_args $version ;;
            pgbouncer) print_pgbouncer_args $version ;;
            *) exit 1;;
        esac
    done

    printf -- "--build-arg CUSTOM_IMAGE_NAME=%s " "$CUSTOM_IMAGE_NAME"
    printf -- "--build-arg CUSTOM_IMAGE_VERSION=%s " "$CUSTOM_IMAGE_VERSION"
}

function generate_command() {
    build_image_name=$1; shift;

    printf -- "docker build "
    echo $build_image_name | tr '-' '\n' | parse_arguments

    for i in "$@"
    do
        printf -- "%s " "$i"
    done
    printf -- ".\\n"
}

function build_custom_if_needed() {
    build_image_name=$1
    full_image_name="$CI_REGISTRY_IMAGE:$build_image_name"

    # This re-uses and builds an existing image if needed
    docker pull --quiet "$full_image" || true
    docker_command=$(generate_command $@ --cache-from="$full_image_name" )
    echo "Building $build_image_name with $docker_command"
    eval $docker_command
}

build_custom_if_needed $@
